#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>

// llvm init
extern char* funcName[];
extern uint64_t rtCallCount;
extern struct {
    uint64_t l;
    uint64_t c;
} callInfo[];

// rt init
struct target{
    uint32_t targetFuncID;
    struct target *next;
};
int lastCallID;
struct target** targetCall;

void initRt(){
    int i;
    targetCall =  malloc((rtCallCount+1)*sizeof(struct target*)); // +1 main puts source
    for(i = 0; i <= rtCallCount; i++){
       targetCall[i] = NULL;
    }
    lastCallID = rtCallCount; // where main puts source
}

void addTarget(struct target **head, int targetID){
    struct target *curr = *head; 
    struct target *tmp;
    struct target *newTarget = malloc(sizeof(struct target)); 

    if(!curr){
        (*head) = newTarget;    
        newTarget->next = NULL; 
        newTarget->targetFuncID = targetID;
        return;
    }

    do{
        if(curr->targetFuncID == targetID)//avoid duplicate
            return;
       tmp = curr;
       curr = curr->next;
    } while(curr);

    newTarget->next = NULL;
    newTarget->targetFuncID = targetID;
    tmp->next = newTarget; 
}


void rtHandleTarget(uint64_t targetFuncID){
    addTarget(&targetCall[lastCallID], targetFuncID);
}

void rtHandleSource(uint64_t calleeID){
    lastCallID = calleeID;
}

void printTargets(struct target *head){
    struct target *curr = head; 
    while(curr){
        //printf("t %s\n", funcName[curr->targetFuncID]);
        fprintf(stderr, "t %s\n", funcName[curr->targetFuncID]);
        curr = curr->next;
    }
}

void printfCallGraph(){
    int i;
    for(i = 0; i < rtCallCount; i++){
        //printf("s %" PRIu64 ":%" PRIu64 "\n", callInfo[i].l, callInfo[i].c);
        fprintf(stderr, "s %" PRIu64 ":%" PRIu64 "\n", callInfo[i].l, callInfo[i].c);
        printTargets(targetCall[i]);
    }
}

void freeWakeLock(int a){}
void getWakeLock(int a){}
