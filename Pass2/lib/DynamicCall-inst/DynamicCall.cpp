#include "llvm/IR/Function.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"

using namespace llvm;

namespace {
    struct DynamicCall : public ModulePass {
        static char ID; // Pass identification, replacement for typeid
        DynamicCall() : ModulePass(ID) {}

        llvm::DenseMap<llvm::Instruction*, uint64_t> idsCall;
        llvm::DenseMap<llvm::Function*, uint64_t> idsFunc;
        bool runOnModule(Module &M) override;
        void handleCaller(Instruction &i, Value *printSource);
        void handleCallee(Function &f, Value *printTarget);
    };
}

static void initRtFunctionArrayName(Module& M, uint64_t numFunc) {
    LLVMContext& Ctx = M.getContext();

    auto* stringTy   = Type::getInt8PtrTy(Ctx);
    auto* tableTy    = ArrayType::get(stringTy, numFunc);
    auto* zero = llvm::ConstantInt::get(llvm::Type::getInt32Ty(Ctx), 0);
    llvm::Value* indices[] = {zero, zero};

    std::vector<Constant*> values;
    for (auto &f: M){
        if (f.isDeclaration()) 
            continue;
        // create const string
        Constant* name = llvm::ConstantDataArray::getString(Ctx, f.getName(), true);
        Type* arrayTy = llvm::ArrayType::get(llvm::Type::getInt8Ty(Ctx) , f.getName().size() + 1);
        GlobalVariable* asStr = new llvm::GlobalVariable(M,
                                                        arrayTy,
                                                        true,
                                                        llvm::GlobalValue::PrivateLinkage,
                                                        name);

        Constant* funcName = llvm::ConstantExpr::getInBoundsGetElementPtr(arrayTy, asStr, indices); 
        values.push_back(funcName);
    }

    auto* functionTable = ConstantArray::get(tableTy, values);
    new GlobalVariable(M,
                        tableTy,
                        false,
                        GlobalValue::ExternalLinkage,
                        functionTable,
                        "funcName");
}

static void initRtCallInfo(Module& M, llvm::ArrayRef<Instruction*> calls){
    LLVMContext& Ctx = M.getContext();

    uint64_t numCalls = calls.size();  

    auto* int64Ty    = Type::getInt64Ty(Ctx);
    Type* fieldTys[] = {int64Ty, int64Ty};
    auto* structTy   = StructType::get(Ctx, fieldTys, false);
    auto* tableTy    = ArrayType::get(structTy, numCalls);

    std::vector<Constant*> values;
    for (auto i : calls){
        auto* line = ConstantInt::get(int64Ty,i->getDebugLoc().getLine(), false);
        auto* col = ConstantInt::get(int64Ty,i->getDebugLoc().getCol(), false);
        Constant* structFields[] = {line, col};
        values.push_back(ConstantStruct::get(structTy, structFields));
    }
    // Calls array
    auto* callsTable = ConstantArray::get(tableTy, values);
    new GlobalVariable(M,
                        tableTy,
                        false,
                        GlobalValue::ExternalLinkage,
                        callsTable,
                        "callInfo");

    // Num call
    auto* numCallsGlobal = ConstantInt::get(Type::getInt64Ty(Ctx), numCalls, false);
    new GlobalVariable(M,
                         int64Ty,
                         true,
                         GlobalValue::ExternalLinkage,
                         numCallsGlobal,
                         "rtCallCount");
}

static DenseMap<Instruction*, uint64_t> genCallId(Module &M, llvm::ArrayRef<Instruction*> calls){
    DenseMap<Instruction*, uint64_t> idMap;
    size_t id = 0;

    for (auto i : calls){
        idMap[i] = id;
        ++id;
    }
    return idMap;
}

static DenseMap<Function*, uint64_t> genFuncId(Module &M){
    DenseMap<Function*, uint64_t> idMap;
    size_t id = 0;

    for (auto &f : M){
        if (!f.isDeclaration()){
            idMap[&f] = id;
            ++id;
        }
    }
    return idMap;
}

bool DynamicCall::runOnModule(Module &M){
    LLVMContext& Ctx = M.getContext();

    std::vector<Instruction*> calls;
    uint64_t numFunc = 0;

    for (auto &f : M){
        if (f.isDeclaration()) 
            continue;

        for (auto& bb : f) {
            for (auto& i : bb) {
                CallSite cs = CallSite(&i);
                if (!cs.getInstruction()) // Verifica se call
                    continue;
                
                auto called = dyn_cast<Function>(cs.getCalledValue()->stripPointerCasts());
                if (!called || !called->isDeclaration() )
                    calls.push_back(&i);
            }
        }
        ++numFunc;
    }

    // get calls ids
    idsCall = genCallId(M, calls);
    idsFunc = genFuncId(M);
    // init rt
    initRtFunctionArrayName(M, numFunc);
    initRtCallInfo(M, calls);
    // set constructor
    Constant *rtConstructor = M.getOrInsertFunction("initRt", Type::getVoidTy(Ctx), false);
    appendToGlobalCtors(M, llvm::cast<Function>(rtConstructor), 0);
    // set destructor
    Constant *rtDestructor = M.getOrInsertFunction("printfCallGraph", Type::getVoidTy(Ctx), false);
    appendToGlobalDtors(M, llvm::cast<Function>(rtDestructor), 0);

    // get rt functions
    Type *Args[] = {
        Type::getInt64Ty(Ctx),  // call id 
    };
    FunctionType *FTy = FunctionType::get(Type::getVoidTy(Ctx), Args, false); 

    Constant *fPrintSource = M.getOrInsertFunction("rtHandleSource", FTy);
    Constant *fPrintTarget= M.getOrInsertFunction("rtHandleTarget", FTy);

    // Add target to functions
    for (auto &f : M){
        if (f.isDeclaration()) 
            continue;
        handleCallee(f, fPrintTarget);
    }
    // add source to calls
    for (auto i: calls){
        handleCaller(*i, fPrintSource);
    }

    return true;
}

void DynamicCall::handleCaller(Instruction &i, Value *printSource){
    IRBuilder<> builder(&i);
    Value *args[] = { builder.getInt64(idsCall[&i]) };
    builder.CreateCall(printSource, args);
}

void DynamicCall::handleCallee(Function &f, Value *printTarget){
    IRBuilder<> builder(&*f.getEntryBlock().getFirstInsertionPt());

    Value *arg[] = { builder.getInt64(idsFunc[&f]) };
    builder.CreateCall(printTarget, arg);
}

char DynamicCall::ID = 0;
static RegisterPass<DynamicCall> X("dynamic-call", "generate call graph ");
