file=$1
filename=$(basename "$file")

actual_path=$(readlink -f "${BASH_SOURCE[0]}")
APP_ROOT=$(dirname $(dirname "$actual_path"))

# generate ir
bitcode=${filename%.*}".bc"
clang -g -O0 -S -emit-llvm $file -o $bitcode

# Pass 1
pass1_file=${filename%.*}".ll.cfgout"
opt -load "${APP_ROOT}"/build/Pass1/lib/libLLVMPassname.so -simplified-cfg $bitcode > /dev/null 2> $pass1_file

# Pass 2
bitcode_rt=${filename%.*}"_perf.bc"
exec_rt=${filename%.*}"_perf"
pass2_file=${filename%.*}".i"
opt -load "${APP_ROOT}"/build/Pass2/lib/DynamicCall-inst/libDynamicCall-lib.so -dynamic-call $bitcode > $bitcode_rt  
clang -O0 "${APP_ROOT}"/build/Pass2/lib/DynamicCall-rt/libDynamicCall-rt.so $bitcode_rt -o $exec_rt -Wl,-rpath,"${APP_ROOT}"/build/Pass2/lib/DynamicCall-rt/
./$exec_rt 2> $pass2_file
