#!/bin/bash

usage()
{
cat << EOF
usage:  $0 [options] [source.c] [-cfg pass1.ll.cfgout pass2.i]
    creates {prog,data}.bin to be input by textbench

OPTIONS:
   -h    Show this message
   -v    Verbose, creates memory map: source.map
   -cfg  Use ll.cfgout & .i , else run pass1 & pass2
EOF
}

if [ $# = 0 ] ; then usage ; exit 1 ; fi

cfg=false
verbose=false
unset debug_flag

while true ; do

    case "$1" in
    -h) usage ; exit 1
        ;;
    -v) verbose=true
        ;;
    -cfg) cfg=true
        ;;
    *)  if [ $cfg = true ]; then
            inp_pass1=${1%.ll.cfgout}
            if [ -z "$inp_pass1" ] || [ ${inp_pass1}.ll.cfgout != $1 ] ; then
                usage ; echo "  invalid option: $1"; exit 1 ; fi
            shift
            inp_pass2=${1%.i}
            if [ -z "$inp_pass2" ] || [ ${inp_pass2}.i != $1 ] ; then
                usage ; echo "  invalid option: $1"; exit 1 ; fi
        else
            inp=${1%.c}
            if [ ${inp}.c != $1 ] ; then
                usage ; echo "  invalid option: $1"; exit 1 ; fi
        fi
        break
        ;;
    esac
    shift
done

if [ $verbose = true ]; then  debug_flag="-v" ; fi

actual_path=$(readlink -f "${BASH_SOURCE[0]}")
APP_ROOT=$(dirname $(dirname "$actual_path"))

if [ $cfg = true ]; then
    pass1_file=${inp_pass1}".ll.cfgout"
    pass2_file=${inp_pass2}".i"
else 
    src=${inp}.c
    filename=$(basename "$src")

    pass1_file=${filename%.*}".ll.cfgout"
    pass2_file=${filename%.*}".i"
    "${APP_ROOT}"/scripts/makeCFG.sh "${src}"
fi

# run bug detector
#"${APP_ROOT}"/BugDetector/bug ${debug_flag} "${pass1_file}" "${pass2_file}"
"${APP_ROOT}"/build/BugDetector/bugDetector ${debug_flag} "${pass1_file}" "${pass2_file}"
