cmake_minimum_required (VERSION 2.6)
project (BugDetector)

if(NOT CMAKE_VERSION VERSION_LESS 3.1)
    set(CMAKE_CXX_STANDARD 11)
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()

add_executable(bugDetector
  Module.cpp
  Function.cpp
  BasicBlock.cpp
  CallSite.cpp
  BugDetector.cpp
)

