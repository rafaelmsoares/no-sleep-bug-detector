#include "CallSite.h"

CallSite::CallSite(int line, int col){
    _line = line;
    _col = col;
    _isWakeLockCall = false;
    _isGetWaveLock = false;
}

int CallSite::getCol(){
    return _col;
}

int CallSite::getLine(){
    return _line;
}

void CallSite::setFuncDest(Function* destFunc){
    _destFunc = destFunc;
}

void CallSite::setDestFuncName(std::string name){
    _destName = name;
}

std::string CallSite::getDestFuncName(){
    return _destName;
}

Function* CallSite::getFuncDest(){
    return _destFunc;
}

void CallSite::setArg(int arg){
    _arg = arg;
}

int CallSite::getArg(){
    return _arg;
}

bool CallSite::isWakeLockCall(){
    return _isWakeLockCall;
}

void CallSite::setWakeLockCall(){
    _isWakeLockCall = true;
}

bool CallSite::isGetWakeLock(){
    return _isGetWaveLock;
}

