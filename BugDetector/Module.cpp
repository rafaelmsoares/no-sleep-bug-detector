#include "Module.h"

void Module::addFunction(Function* f){
    _functions.push_back(f);
}


std::vector<Function*> *Module::getFunctions(){
    return &_functions;
}

Function* Module::getFunctionByName(std::string name){
    std::vector<Function*>::iterator f;

    for(f = _functions.begin(); f != _functions.end(); ++f){
        if ((*f)->getName() == name)
            return *f;
    }
}

void Module::initComponentDefMap(){
    for(auto comp: _lockComponents){
        componentDefMap.insert(make_pair(comp, std::vector<bool>(_numDefs)));
    }
}

// TODO: INDIRECT CALL to lock (args?)
//
void Module::resolveCalls(std::map<std::pair<int,int>, std::vector<Function*>> &indirectCallProfile){
    std::vector<Function*>::iterator f;

    for(f = _functions.begin(); f != _functions.end(); ++f){
        std::vector<BasicBlock*> *BBs = (*f)->getBBs();

        // iterate through blocks
        std::vector<BasicBlock*>::size_type cfg_size = BBs->size();
        for (std::vector<BasicBlock*>::size_type bb_i = 1; bb_i < cfg_size; ++bb_i){
            BasicBlock* bb = (*BBs)[bb_i];

            // iterate through calls
            std::vector<CallSite>* calls =  bb->getCalls();
            for (std::vector<CallSite*>::size_type cs_i = 0; cs_i < calls->size(); ){
                CallSite &cs = (*calls)[cs_i];

                if(!cs.getDestFuncName().empty()) { // direct call // MELHORAR!!

                    Function *f_dest = getFunctionByName(cs.getDestFuncName());
                    cs.setFuncDest(f_dest);

                    // reaching def (we add this only here since we cannot have indirect call to lock)
                    if (cs.isWakeLockCall()){
                        int comp = cs.getArg();
                        componentDefMap[comp][cs._defID] = true;
                        // used to print path
                        defCallSiteMap[cs._defID] = make_pair(&cs, bb);
                    }
                    // call graph
                    (*f)->addSucessor(f_dest);
                    // next call
                    ++cs_i;
                    
                    //cout << "found direct call: " << cs.getLine() << " "
                    //     << cs.getCol() << " to: " << cs.getFuncDest()->getName()  << endl;

                } else { // indirect call

                    std::vector<Function*> &targets = indirectCallProfile[make_pair(cs.getLine(), cs.getCol())];
                    if(targets.size() > 0){ // one target
                        if (targets.size() == 1) {

                            Function *f_dest = targets.front();
                            cs.setFuncDest(f_dest);
                            // call graph
                            (*f)->addSucessor(f_dest);
                            // next call
                            ++cs_i;
                            
                            //cout << "found indirect call with one target: " << cs.getLine() << " "
                            //     << cs.getCol() << " to: " << cs.getFuncDest()->getName()  << endl;

                        } else { // multiple targets -> break bb
                            //cout << "found indirect call with multiple targets: " 
                            //    << cs.getLine() << " " << cs.getCol() << endl;

                            BasicBlock *BB_i = bb;
                            BasicBlock *BB_j = new BasicBlock();

                            int nextAvailableID =  BBs->size();
                            BB_j->setID(nextAvailableID++);
                            BB_j->f = bb->f;
                            //debug
                            BB_i->_name = "break i - indirect";
                            BB_j->_name = "break j - indirect";

                            for(auto suc: *bb->getSucessors()){
                                BB_j->addSucessor(suc);
                                suc->replacePredecessor(bb, BB_j);
                            }
                            bb->removeSucessors();
                            
                            // add instructions to BB_j
                            for (std::vector<CallSite*>::size_type cs_j = cs_i+1; cs_j < calls->size(); ++cs_j){
                                BB_j->addCallSite((*calls)[cs_j]);
                            }
                            // remove instructions from BB_i
                            calls->erase(calls->begin()+cs_i, calls->end());
                            // add new bb
                            BBs->push_back(BB_j);
                            // increase cfg_szie so for process new bb
                            ++cfg_size;
                            // for everty target
                            for(auto t: targets){
                                BasicBlock *new_bb = new BasicBlock();
                                new_bb->setID(nextAvailableID++);
                                BB_i->addSucessor(new_bb); 
                                BB_j->addPredecessor(new_bb);
                                new_bb->addSucessor(BB_j);
                                new_bb->addPredecessor(BB_i);
                                // add call to block
                                //CallSite new_cs = new CallSite(cs.getLine(), cs.getCol());
                                CallSite new_cs(cs.getLine(), cs.getCol()); // PROBLEM?? alocado na stack?
                                new_cs.setFuncDest(t);
                                new_bb->addCallSite(new_cs);
                                // add to cfg
                                BBs->push_back(new_bb);
                                // call graph
                                (*f)->addSucessor(t);
                                // 
                                new_bb->f = bb->f;
                            }
                            // done processing calls from current block
                            break;

                        }
                    } else { // profile could say what the target is, remove since it doesnt add info to the dfa
                        //cout << "profile couldnt say what the target is: "
                        //        << cs.getLine() << " " << cs.getCol() << endl;
                        calls->erase(calls->begin()+cs_i);
                    }
                }
            }
        }
    }
}

// set functior pointer for calls
void Module::printCFG(){
    std::vector<Function*>::iterator f;
    for(f = _functions.begin(); f != _functions.end(); ++f){
        cout << "nome: " << (*f)->getName() << " " << *f << endl;

        std::vector<BasicBlock*>::iterator bb;
        std::vector<BasicBlock*>*BBs = (*f)->getBBs();
        
        for(bb = BBs->begin(); bb != BBs->end(); ++bb){
            cout << "\tbb: " << (*bb)->getID() << " " << *bb << endl;
            cout << "\t\tf: " << (*bb)->f << endl;

            std::vector<CallSite>*CSs = (*bb)->getCalls();
            std::vector<CallSite>::iterator cs;
            for(cs = CSs->begin(); cs != CSs->end(); ++cs){
                cout << "\t\tcall: " << (*cs).getLine() << ":" << (*cs).getCol()
                    << " to: " << (*cs).getFuncDest()->getName() << endl;
            }
            cout << "\t\t----" << endl; 
            for (auto suc : *(*bb)->getSucessors()){
                cout << "\t\tsuc: " << suc->getID() << " (" << suc << ")" << endl;
            }
            for (auto pred : *(*bb)->getPredecessors()){
                cout << "\t\tpred: " << pred->getID() << " (" << pred << ")" << endl;
            }

        }
    }
}

