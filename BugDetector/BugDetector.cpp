#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <stack>
#include <queue>
#include <algorithm> 

#include "Module.h"
#include "Function.h"
#include "BasicBlock.h"
#include "CallSite.h"

using namespace std;

struct Context{
    BasicBlock* entry;
    BasicBlock* exit;
    Function* f;
};

Module MCondensed;
Module M;

bool debug = false;

void parser(std::string file){

    // open file
    std::ifstream infile(file.c_str());

    std::string line;
    while (std::getline(infile, line)){
        std::istringstream iss(line);
        int nFunc;
        iss >> nFunc;
        int defID = 0;

        for (int i = 0; i < nFunc; i++){

            std::getline(infile, line);
            std::istringstream iss(line);

            std::string funcName;
            int nBB, nEdge;
            iss >> funcName >> nBB >> nEdge; 

            Function *fun = new Function(funcName, nBB+1); // +1 = entry

            // read basic blocks
            for (int j = 0; j < nBB; j++){
                BasicBlock *bb = new BasicBlock();

                std::getline(infile, line);
                std::istringstream iss(line);

                int bbID, bbNCall;
                iss >> bbID >> bbNCall;
                bb->setID(bbID);
                bb->f = fun;

                // read calls
                for (int k = 0; k < bbNCall; k++){
                    std::string l, c, tipo;
                    std::string funcDest;
                    std::getline(infile, l, ':');
                    std::getline(infile, c, ':');
                    std::getline(infile, tipo);

                    CallSite cs(std::stoi(l), std::stoi(c));

                    if (tipo != "i"){
                        std::string funcDest = tipo.substr(2, tipo.find(" ")-2);

                        cs.setDestFuncName(funcDest);

                        if (funcDest == "getWakeLock" || funcDest == "freeWakeLock"){
                            std::string arg = tipo.substr(tipo.find(" "));
                            
                            cs.setArg(std::stoi(arg));
                            cs._defID = defID++;
                            cs.setWakeLockCall();
                            M._lockComponents.insert(std::stoi(arg));

                            if (funcDest == "getWakeLock")
                                cs._isGetWaveLock = true;
                        }
                    }
                    bb->addCallSite(cs);
                }

                fun->addBB(bb, bbID);
            }
            // read edges
            for (int j = 0; j < nEdge; j++){
                std::getline(infile, line);
                std::string u = line.substr(0, line.find("->"));
                std::string v = line.substr(line.find("->") + 2);

                BasicBlock* bb_orig = fun->getBBByID(std::stoi(u));
                BasicBlock* bb_dest = fun->getBBByID(std::stoi(v));
                bb_orig->addSucessor(bb_dest);
                bb_dest->addPredecessor(bb_orig);
            }
            // add function to module
            M.addFunction(fun);
            M._numDefs = defID;
        }
    }

}


void parser2(std::string file, std::map<std::pair<int,int>, std::vector<Function*>> &indirectCallProfile){
    std::pair <int,int> source;
    // open file
    std::ifstream infile(file.c_str());
    std::string line;
    std::vector<Function*> targets;

    while (std::getline(infile, line)){
        if(!line.empty()){ 
            if (line.at(0) == 's'){ // source

                if(targets.size()){ // end of previous source
                    indirectCallProfile[make_pair(source.first, source.second)] = targets;
                    targets.clear(); // Complexity?
                }

                int colon_index = line.find(":");
                std::string l = line.substr(2, colon_index-2);
                std::string c = line.substr(colon_index+1, line.size()-1);
                source = std::make_pair(std::stoi(l),std::stoi(c));
            } else { // target
                std::string funName = line.substr(2, line.size()-1);
                Function *f = M.getFunctionByName(funName);
                targets.push_back(f);
            }
        }
    }

    if(targets.size()){ // flush left over
        indirectCallProfile[make_pair(source.first, source.second)] = targets;
    }

}


struct Context *callExpanded(Function *f, std::vector<struct Context> &callStack){
    auto pred = [f](const struct Context &ctx) {
        return ctx.f == f;
    };

    auto it = std::find_if(callStack.begin(), callStack.end(), pred);
    if (it != callStack.end()){
        auto index = std::distance(callStack.begin(), it);
        return &callStack[index];
    }
    return NULL;
}

void condenseSCC(Function* f_condensed,
                    std::vector<BasicBlock*> *BBs,
                    std::vector<struct Context> &callStack,
                    std::vector<Function*> SCC){
    // for every bb
    std::vector<BasicBlock*>::size_type cfg_size = BBs->size();
    for (std::vector<BasicBlock*>::size_type bb_i = 0; bb_i < cfg_size; ++bb_i){
        BasicBlock* curr_bb = (*BBs)[bb_i];

        // for every call in bb
        std::vector<CallSite>* calls =  curr_bb->getCalls();
        for (std::vector<CallSite*>::size_type cs_i = 0; cs_i < calls->size(); ++cs_i){
            CallSite curr_call = (*calls)[cs_i];
                
            // if dest of call is in SCC
            Function* f_dest = curr_call.getFuncDest();
            if (std::find(SCC.begin(), SCC.end(), f_dest) != SCC.end()){

                BasicBlock *destBBEntry;
                BasicBlock *destBBExit;

                // if call already expanded
                struct Context *context = callExpanded(f_dest, callStack);
                if(context){
                   destBBEntry = context->entry; 
                   destBBExit = context->exit; 
                } else { // break bb
                    // copy bbs
                    struct Context newContext;
                    std::vector<BasicBlock*>* dest_BBs_copy = f_dest->getBBsCopy();
                    destBBEntry = dest_BBs_copy->front();
                    destBBExit = dest_BBs_copy->back();
                    // make new context 
                    newContext.entry = destBBEntry;
                    newContext.exit = destBBExit;
                    newContext.f = f_dest;
                   // recursive call 
                    callStack.push_back(newContext);
                    condenseSCC(f_condensed, dest_BBs_copy, callStack, SCC);
                    callStack.pop_back();
                }

                BasicBlock *BB_i = curr_bb;
                BasicBlock *BB_j = new BasicBlock();

                //debug
                BB_i->_name = "break i";
                BB_j->_name = "break j";

                for(auto suc: *curr_bb->getSucessors()){
                    BB_j->addSucessor(suc);
                    suc->replacePredecessor(curr_bb, BB_j);
                }
                curr_bb->removeSucessors();
                
                // add instructions to BB_j
                for (std::vector<CallSite*>::size_type cs_j = cs_i+1; cs_j < calls->size(); ++cs_j){
                    BB_j->addCallSite((*calls)[cs_j]);
                }
                // remove instructions from BB_i
                calls->erase(calls->begin()+cs_i, calls->end());
                // add new bb
                BBs->push_back(BB_j); // we restore exit to last pos. later on
                // increase cfg_szie so for process new bb
                ++cfg_size;
                // BB_i has only on sucessor
                BB_i->addSucessor(destBBEntry); 
                destBBEntry->addPredecessor(BB_i);
                // BB_j has only onde predecessor
                destBBExit->addSucessor(BB_j);
                BB_j->addPredecessor(destBBExit);
                // set isomorphism info
                BB_i->orig_equivalent->mod_equivalent = BB_j;
                BB_j->orig_equivalent = BB_i->orig_equivalent;
                // done processing calls from current block
                break;
            }
        }
        f_condensed->addBB(curr_bb);
    }
}

void stronglyConnected(Function *f,
                        int &i,
                        std::stack<Function*>&s,
                        std::map<Function*, Function*> &mapMToMod){
    (*f).setIndex(i); 
    (*f).setLowLink(i); 
    ++i;
    s.push(f);
    (*f).inStack = true;

    std::set<Function*> *sucessors = (*f).getSucessors();

    // Consider successors of v
    for (auto suc : *sucessors){
        if((*suc).getIndex() == -1){
            stronglyConnected(suc, i, s, mapMToMod);
            (*f).setLowLink(min((*f).getLowLink(), (*suc).getLowLink()));
        } else if ((*suc).inStack) {
            (*f).setLowLink(min((*f).getLowLink(), (*suc).getIndex()));
        }
    } 
    // If v is a root node, pop the stack and generate an SCC
    if ((*f).getLowLink() == (*f).getIndex()){
        // start a new strongly connected component
        std::vector<Function*> SCC;
        Function* w;
        std::stringstream ss;
    
        w = s.top();
        Function *f_entry = w;
        Function *f_condensed = new Function();
        std::vector<BasicBlock*> *bbs_copy_fentry = f_entry->getBBsCopy();
        // save exit object
        BasicBlock* exit_bb = bbs_copy_fentry->back();

        do {
            w = s.top();
            s.pop();
            w->inStack = false;
            ss << w->getName();
            // add w to current strongly connected component
            SCC.push_back(w);
            // add to map
            mapMToMod[w] = f_condensed;
        } while (w != f);

        // output the current strongly connected component
        if(debug) cout << "SCC name: " << ss.str() << endl;
        // Compute the condensed cfg of SSC
        f_condensed->setName(ss.str());
        std::vector<struct Context> callStack;

        struct Context newContext;
        newContext.entry = bbs_copy_fentry->front();
        newContext.exit = bbs_copy_fentry->back();
        newContext.f = f_entry;

        callStack.push_back(newContext);
        condenseSCC(f_condensed, bbs_copy_fentry, callStack, SCC);

        // restore exit to last position
        // find exit index
        std::vector<BasicBlock*> *BBs_fCondensed = f_condensed->getBBs();
        std::vector<BasicBlock*>::iterator it;
        it = find(BBs_fCondensed->begin(), BBs_fCondensed->end(), exit_bb);
        // swap
        ptrdiff_t exit_index =  it - BBs_fCondensed->begin();
        BasicBlock *aux = BBs_fCondensed->back();
        BBs_fCondensed->back() = (*BBs_fCondensed)[exit_index];
        (*BBs_fCondensed)[exit_index] = aux;

        MCondensed.addFunction(f_condensed);
    }
}

void condenseCallGraph(Module &M){
    int index = 0;    
    std::stack<Function*> s;
    std::map<Function*, Function*> mapMToMod;

    std::vector<Function*> *functions = M.getFunctions();
    for (auto f : *functions){
        if ((*f).getIndex() == -1) 
            stronglyConnected(f, index, s, mapMToMod);
    }
    // resolve edges and calls
    Function*f_cond, *suc_cond;
    for (auto f : *functions){
        // edges
        f_cond = mapMToMod[f];
        for (auto suc : *f->getSucessors()){
            suc_cond = mapMToMod[suc];
            if (f_cond != suc_cond)
                f_cond->addSucessor(suc_cond);
        }
    }
    // resolve calls
    for (auto f : *MCondensed.getFunctions()){
        for (auto bb: *f->getBBs()){
            for(auto &call: *bb->getCalls()){
                call.setFuncDest(mapMToMod[call.getFuncDest()]);
            }
        }
    }
}

std::vector<bool> operator|(std::vector<bool> A, const std::vector<bool>& B){
    std::vector<bool>::iterator itA = A.begin();
    std::vector<bool>::const_iterator itB = B.begin();

    while (itA < A.end())
        *(itA._M_p ++) |= *(itB._M_p ++); // word-at-a-time bitwise operation

    return A;
}


std::vector<bool> operator&(std::vector<bool> A,const std::vector<bool>& B){
    std::vector<bool>::iterator itA = A.begin();
    std::vector<bool>::const_iterator itB = B.begin();

    while (itA < A.end())
        *(itA._M_p ++) &= *(itB._M_p ++); // word-at-a-time bitwise operation

    return A;
}

std::vector<bool> operator^(std::vector<bool> A,const std::vector<bool>& B){
    std::vector<bool>::iterator itA = A.begin();
    std::vector<bool>::const_iterator itB = B.begin();

    while (itA < A.end())
        *(itA._M_p ++) ^= *(itB._M_p ++); // word-at-a-time bitwise operation

    return A;
}

void printBitVector(std::vector<bool> bitvector){
    for (int i=0; i < bitvector.size(); i++)
        cout << bitvector[i] ;
    cout << endl;
}


// gen: 1 -> gen def
// kill: 0 -> kill def
void computeGenKill(Function *f, std::map<int, std::vector<bool> >& componentDefMap, int numDefs){

    for (auto bb: *f->getBBs()){
        bb->initRDSets(numDefs);

        std::vector<bool>& gen_bb = bb->rd_gen;
        std::vector<bool>& kill_bb = bb->rd_kill;
        std::vector<bool> kill_s(bb->rd_kill.size());

        for (auto call: *bb->getCalls()){
            if (call.isWakeLockCall()){
                kill_s = componentDefMap[call.getArg()];  
                kill_s.flip();

                kill_s[call._defID] = true;
                kill_bb = kill_bb & kill_s;
                
                gen_bb = gen_bb & kill_s;
                gen_bb[call._defID] = true;
            } else { // inteprodecural
                Function *f_dest = call.getFuncDest();                

                kill_s = f_dest->getExit()->mk_in;
                kill_bb = kill_bb & kill_s;

                gen_bb = (gen_bb & kill_s) | f_dest->getExit()->rd_in;
            }
        }
        if(debug){
            cout << bb->getID() << endl;
            cout << "gen=";
            printBitVector(gen_bb);
            cout << "kill=";
            printBitVector(kill_bb);
        }
    }
}

void runMustKill(Function *f){
    std::queue<BasicBlock*> workList;
    std::vector<bool> old_out;

    for (auto bb: *f->getBBs()){
        workList.push(bb);
    }

    while(!workList.empty()){
        BasicBlock *bb = workList.front();
        workList.pop();

        // in
        if (bb->getPredecessors()->size()){
            bb->mk_in = bb->getPredecessors()->front()->mk_out; // init val of mk_in is set all true
                                                                // 
        }
        for (auto pred : *bb->getPredecessors()){
            bb->mk_in =  bb->mk_in | pred->mk_out; // intersection
        }
        // out
        old_out = bb->mk_out;
        bb->mk_out = bb->mk_in & bb->rd_kill; // union

        old_out = old_out ^ bb->mk_out;
        if(!std::all_of(old_out.begin(), old_out.end(), [](bool v) {return !v;} )){ // all false
            for (auto suc: *bb->getSucessors()){
                workList.push(suc);
            }
        }
    }

    if(debug){
        cout << "MUST KILL" << endl;
        for (auto bb: *f->getBBs()){
            cout << bb->getID() << endl;
            cout << "in=";
            printBitVector(bb->mk_in);
            cout << "out=";
            printBitVector(bb->mk_out);
        }
    }
}


void runReachingDefinitions(Function *f){
    std::queue<BasicBlock*> workList;
    std::vector<bool> old_out;

    for (auto bb: *f->getBBs()){
        workList.push(bb);
    }

    while(!workList.empty()){
        BasicBlock *bb = workList.front();
        workList.pop();

        // in
        for (auto pred : *bb->getPredecessors()){
            bb->rd_in =  bb->rd_in | pred->rd_out;
        }
        // out
        old_out = bb->rd_out;
        bb->rd_out = bb->rd_gen | (bb->rd_in & bb->rd_kill);

        old_out = old_out ^ bb->rd_out;
        if(!std::all_of(old_out.begin(), old_out.end(), [](bool v) {return !v;} )){ // all false
            for (auto suc: *bb->getSucessors()){
                workList.push(suc);
            }
        }
    }

    if(debug){
        cout << "REACHING DEF" << endl;
        for (auto bb: *f->getBBs()){
            cout << bb->getID() << endl;
            cout << "in=";
            printBitVector(bb->rd_in);
            cout << "out=";
            printBitVector(bb->rd_out);
        }
    }
}

void topologicalSort(Function *f){
    f->ts_visited = true;
    for (auto suc : *f->getSucessors()){
        if (!suc->ts_visited){
            topologicalSort(suc);
        }
    }
    
    if(debug) cout << "top: " << f->getName() << endl;

    computeGenKill(f, M.componentDefMap, M._numDefs);
    runReachingDefinitions(f);
    runMustKill(f);
}

void runTopologicalDFA(){
    std::vector<Function*> *functions = MCondensed.getFunctions();
    for (auto f : *functions){
        if (!f->ts_visited){
            topologicalSort(f);
        }
    }
}

void printPath(BasicBlock *font, std::stringstream& buffer){
    BasicBlock *bb = font; 

    buffer << font->f->getName() << endl;
    while (bb && bb->parent){
        if (bb->f != bb->parent->f){
            buffer << bb->parent->f->getName() << endl;
        } else if (bb->parent != bb->f->getExit()) { // do not print exit
            buffer << bb->getID() << "->" << bb->parent->getID() << endl; 
        }
        bb = bb->parent;
    }
}


BasicBlock* runBFS(BasicBlock *entry, int defID){
    std::queue<BasicBlock*> q;

    entry->visited = true;
    q.push(entry);

    //cout << "=== def " << defID << endl;
    while(!q.empty()){
        BasicBlock *bb = q.front();   
        q.pop();

        if(bb->hasDef(defID))
            return bb;

        //cout << "fronteira de : " << bb->getID() << endl;
        for (auto &suc: *bb->getPredecessors()){
            if (!suc->visited && suc->mod_equivalent->rd_out[defID]){
                suc->visited = true;
                suc->parent = bb;
                q.push(suc);
                //cout << suc->getID() << endl;
            }
        }
        for (auto &call: *bb->getCalls()){
            BasicBlock* suc = call.getFuncDest()->getExit();
            if (!suc->visited && suc->mod_equivalent->rd_out[defID]){
               suc->visited = true;
               suc->parent = bb;
               q.push(suc);
                //cout << call.getFuncDest()->getName() << ": " << suc->getID() << endl;
            }
        }
    }
    cout << "DIDNT FIND DEF" << endl;
    return NULL;

}

void runBugDetector(){

    std::stringstream bufferPaths;
    Function* fMain_orig = M.getFunctionByName("main");
    Function* f_test = MCondensed.getFunctionByName("main");
    std::vector<bool> rd = f_test->getExit()->rd_in;
    int numBugs = 0;
    for (int i=0; i < rd.size(); i++){
        if (rd[i]){
            CallSite* cs = M.defCallSiteMap[i].first;
            if (cs->isGetWakeLock()){
                BasicBlock *font = runBFS(fMain_orig->getExit(), i);
                if(font){
                    numBugs++;
                    bufferPaths << "bug " << cs->getLine() << ":" << cs->getCol() << endl;
                    printPath(font, bufferPaths);
                }else{
                    rd[i] = false;
                }
                // unmark visited nodes 
                for (auto f : *M.getFunctions()){
                    for (auto bb: *f->getBBs()){
                        bb->visited = false;
                    }
                }
            }
        }
    }
    if(numBugs){
        cout << "sim" << endl;
        cout << numBugs << endl;
        cout << bufferPaths.rdbuf();
    } else {
        cout << "não" << endl;
    }

}

int main(int argc, char* argv[]){

    if (argc < 3){
        std::cerr << "Usage: bug [-v] arq.ll.cfgout arq.i" << endl;
        return 1;
    }
    
    string pass1_file, pass2_file;
    if (!strcmp(argv[1],"-v")) { // debug mode
        cout << "===== DEBUG MODE =====" << endl;
        debug = 1;
        pass1_file = argv[2];
        pass2_file = argv[3];
    } else { // normal mode
        pass1_file = argv[1];
        pass2_file = argv[2];
    }

    // Read simplified CFG (Pass 1 - Static Analysis)
    parser(pass1_file); 

    // Read call info. (Pass 2 - Dynamic Analysis) 
    std::map<std::pair<int,int>, std::vector<Function*>> indirectCallProfile;
    parser2(pass2_file, indirectCallProfile);
   
    // Initialize component def map
    M.initComponentDefMap();

    // Resolve calls - set function pointer to call sites & build call graph 
    M.resolveCalls(indirectCallProfile);

    // Add ENTRY & EXIT to cfg of functions
    for (auto f : *M.getFunctions()){
        f->genEntryExit();
    }
    
    // Condense call graph - collapse SCC 
    condenseCallGraph(M);
    
    // Run DFA in topolofical order
    runTopologicalDFA(); 

    if(debug){
        cout << "-- CFG --" << endl;
        M.printCFG();
        cout << "-- CONDENSED CFG --" << endl;
        MCondensed.printCFG();
        cout << "===================" << endl;
    }

    // Run bug detector
    runBugDetector();

}
