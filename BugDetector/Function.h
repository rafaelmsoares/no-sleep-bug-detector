#ifndef FUNCTION_H
#define FUNCTION_H

#include <vector>
#include <string>
#include <iostream>
#include <set>
#include "BasicBlock.h"

using namespace std;
class BasicBlock;

class Function{
    public:
        Function();
        Function(std::string, int);
        void setName(string name);
        std::string getName();
        void addBB(BasicBlock *bb, int pos);
        void addBB(BasicBlock *bb);
        BasicBlock* getBBByID(int id);
        std::vector<BasicBlock*> *getBBs();
        std::vector<BasicBlock*> *getBBsCopy();
        Function* getClone();
        void addSucessor(Function* f);
        std::set<Function*> *getSucessors();
        void genEntryExit();
        BasicBlock* getEntry();
        BasicBlock* getExit();
        void printBBs(); 
        //Tarjan's algorithm
        int getIndex();
        int getLowLink();
        void setIndex(int);
        void setLowLink(int);
        bool inStack = false;
        // Topological Sorting
        bool ts_visited;

    private:
        std::string _name;
        //std::vector<int> basicBlocks;
        std::vector<BasicBlock*> _basicBlocks;
        std::set<Function*> _sucessors; // Call Graph
        //Tarjan's algorithm
        int index_;
        int lowLink;
        //  
        void copyBBs(std::vector<BasicBlock*> *bbsCopy);
};


#endif


