#ifndef MODULE_H
#define MODULE_H

#include "Function.h"
#include <stack>
#include <map>

class Module {
    public:
        std::vector<Function*> *getFunctions();
        void addFunction(Function* f);
        Function* getFunctionByName(std::string);
        void resolveCalls(std::map<std::pair<int,int>, std::vector<Function*>> &indirectCallProfile);
        //debug
        void printCFG();
        // rd
        int _numDefs;
        std::set<int> _lockComponents;
        void initComponentDefMap();
        std::map<int, std::vector<bool> > componentDefMap;
        std::map<int, std::pair<CallSite*, BasicBlock*> > defCallSiteMap;

    private:
        std::vector<Function*> _functions; 
};

#endif
