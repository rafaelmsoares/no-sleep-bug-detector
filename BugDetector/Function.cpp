#include "Function.h"

using namespace std;

Function::Function(std::string name, int numBBs): _basicBlocks(numBBs), _name(name), index_(-1), lowLink(-1),
                                                    ts_visited(false) {}

Function::Function() {}

void Function::setName(string name){
    _name = name;
}
std::string Function::getName(){
    return _name;
}

void Function::addBB(BasicBlock *bb, int pos) {
    _basicBlocks[pos] = bb;    
}

void Function::addBB(BasicBlock *bb) {
    _basicBlocks.push_back(bb);    
}

BasicBlock* Function::getBBByID(int id){
    return _basicBlocks[id];
}

std::vector<BasicBlock*> *Function::getBBs(){
    return &_basicBlocks;
}

std::set<Function*> *Function::getSucessors(){
    return &_sucessors;
}

void Function::addSucessor(Function* f){
    _sucessors.insert(f);
}

void Function::printBBs(){     
    std::vector<BasicBlock*>::iterator b;
    for(b = _basicBlocks.begin(); b != _basicBlocks.end(); ++b){
        std::cout << "bb: " << (*b)->getID() << endl;
    }
}


void Function::setIndex(int index){
    index_ = index;
}

void Function::setLowLink(int ll){
    lowLink = ll;
}

int Function::getIndex(){
    return index_;
}

int Function::getLowLink(){
    return lowLink;
}

void Function::genEntryExit(){
    BasicBlock *entry = new BasicBlock();
    BasicBlock *exit = new BasicBlock();

    entry->setID(0);
    exit->setID(_basicBlocks.size());

    exit->f = this;
    entry->f = this;
    
    for (std::vector<BasicBlock*>::size_type i = 1; i < _basicBlocks.size(); ++i){
        if (!_basicBlocks[i]->getSucessors()->size()){
            _basicBlocks[i]->addSucessor(exit);
            exit->addPredecessor(_basicBlocks[i]);
        }
        
        if (!_basicBlocks[i]->getPredecessors()->size()){
            entry->addSucessor(_basicBlocks[i]);
            _basicBlocks[i]->addPredecessor(entry);
        }
    }

    //debug
    entry->_name = "entry";
    exit->_name = "exit";

    _basicBlocks.front() = entry; 
    _basicBlocks.push_back(exit);

}


void Function::copyBBs(std::vector<BasicBlock*> *bbsCopy){

    for (std::vector<BasicBlock*>::size_type i = 0; i < _basicBlocks.size(); ++i){
        BasicBlock *bb = _basicBlocks[i];
        BasicBlock *bb_copy = new BasicBlock();
        // copy id
        bb_copy->setID(bb->getID());
        // copy instructions
        for (auto call : *bb->getCalls()){
            bb_copy->addCallSite(call);
        }
        // copy name (begug)
        bb_copy->_name = bb->_name;
        // copy func that contains (necessary print path)
        bb_copy->f = this;
        // ref to the to mod (necessary print path)
        bb->mod_equivalent = bb_copy;
        bb_copy->orig_equivalent = bb;

        (*bbsCopy)[i] = bb_copy;
    }
    // copy predecessors & sucessors
    for (std::vector<BasicBlock*>::size_type i = 0; i < _basicBlocks.size(); ++i){
        BasicBlock *bb = _basicBlocks[i];
        BasicBlock *bb_copy = (*bbsCopy)[i];
        // sucessor
        std::vector<BasicBlock*> *sucessors = bb->getSucessors();
        for (std::vector<BasicBlock*>::size_type j = 0; j < sucessors->size(); ++j){ 
            std::vector<BasicBlock*>::size_type suc_id = (*sucessors)[j]->getID(); 
            bb_copy->addSucessor((*bbsCopy)[suc_id]);
        }
        // predecessors
        std::vector<BasicBlock*> *predecessors= bb->getPredecessors();
        for (std::vector<BasicBlock*>::size_type j = 0; j < predecessors->size(); ++j){ 
            std::vector<BasicBlock*>::size_type pred_id = (*predecessors)[j]->getID(); 
            bb_copy->addPredecessor((*bbsCopy)[pred_id]);
        }
    }

}

std::vector<BasicBlock*> *Function::getBBsCopy(){
    std::vector<BasicBlock*> *bbsCopy = new std::vector<BasicBlock*>(_basicBlocks.size());
    copyBBs(bbsCopy);

    return bbsCopy;
}

Function* Function::getClone(){
    Function *f_clone = new Function(_name, _basicBlocks.size());
    std::vector<BasicBlock*> *bbsClone = f_clone->getBBs();
    copyBBs(bbsClone);

    return f_clone;
}

BasicBlock* Function::getEntry(){
    return _basicBlocks.front();
}

BasicBlock* Function::getExit(){
    return _basicBlocks.back();
}

