#ifndef CALLSITE_H
#define CALLSITE_H

#include "Function.h"
class Function;
class CallSite{
    private:
        int _line, _col;
        Function* _destFunc;
        std::string _destName;
        int _arg;
        bool _isWakeLockCall; // melhor para OO

    public:
        CallSite(int, int);
        int getLine();
        int getCol();
        void setFuncDest(Function*);
        void setDestFuncName(std::string name);
        std::string getDestFuncName();
        Function* getFuncDest();
        void setArg(int arg); 
        int getArg();
        bool isWakeLockCall();
        bool isGetWakeLock();
        void setWakeLockCall();
        bool _isGetWaveLock;
        int _defID; // reaching def
};

#endif
