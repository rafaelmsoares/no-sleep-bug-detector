#include "BasicBlock.h"

void BasicBlock::setID(int id){
    _id = id;
    //debug
   _name = std::to_string(id); 
}

int BasicBlock::getID(){
    return _id;
}

void BasicBlock::addCallSite(CallSite cs){
    _callSites.push_back(cs);
}

void BasicBlock::addSucessor(BasicBlock* bb){
    _sucessorBBs.push_back(bb); 
}

void BasicBlock::addPredecessor(BasicBlock* bb){
    _predecessorBBs.push_back(bb); 
}

std::vector<CallSite>* BasicBlock::getCalls(){
    return &_callSites;
}

std::vector<BasicBlock*> *BasicBlock::getSucessors(){
    return &_sucessorBBs;
}

std::vector<BasicBlock*> *BasicBlock::getPredecessors(){
    return &_predecessorBBs;
}

void BasicBlock::replacePredecessor(BasicBlock *orig_bb, BasicBlock *new_bb){
    std::vector<BasicBlock*>::iterator it = std::find(_predecessorBBs.begin(), _predecessorBBs.end(), orig_bb);
    if (it != _predecessorBBs.end()){
        auto orig_index = std::distance(_predecessorBBs.begin(), it);
        _predecessorBBs[orig_index] = new_bb;
    }
}

void BasicBlock::removeSucessors(){
    _sucessorBBs.clear();
}

void BasicBlock::initRDSets(int numDefs){
    for(int i = 0; i < numDefs; i++){
        // rd
        rd_gen.push_back(false);
        rd_kill.push_back(true);
        rd_in.push_back(false); 
        rd_out.push_back(false);
        // mk
        mk_in.push_back(true);
        mk_out.push_back(true);
    }
}

bool BasicBlock::hasDef(int def){
    for(auto &call: _callSites){
        if(call.isWakeLockCall() && call._defID == def)
            return true;
    }
    return false;
}
