#ifndef BASICBLOCK_H
#define BASICBLOCK_H

#include "CallSite.h"
#include "Function.h"
#include <algorithm> 

class CallSite;
class Function;
class BasicBlock{
    public:
        //BasicBLock(int);
        void setID(int id);
        int getID();
        void addCallSite(CallSite cs);
        void addSucessor(BasicBlock* bb);
        void addPredecessor(BasicBlock* bb);
        std::vector<CallSite>* getCalls();
        std::vector<BasicBlock*> *getSucessors();
        std::vector<BasicBlock*> *getPredecessors();
        void removeSucessors();
        void replacePredecessor(BasicBlock*, BasicBlock*);
        bool hasDef(int);
        // debug
        std::string _name;
        // rd
        std::vector<bool> rd_gen;
        std::vector<bool> rd_kill;
        std::vector<bool> rd_in; 
        std::vector<bool> rd_out;
        // must kill
        std::vector<bool> mk_in; 
        std::vector<bool> mk_out;
        // data flow
        void initRDSets(int);
        // bfs
        bool visited;
        BasicBlock *parent;
        Function *f;
        BasicBlock *mod_equivalent;
        BasicBlock *orig_equivalent;

    private:
        int _id;
        std::vector<CallSite> _callSites;
        std::vector<BasicBlock*> _sucessorBBs;
        std::vector<BasicBlock*> _predecessorBBs;
};

#endif
