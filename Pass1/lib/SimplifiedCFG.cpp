#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Value.h"
#include "llvm/Support/Casting.h"
#include "llvm/IR/Constants.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;


namespace {

  struct SimplifiedCFG : public ModulePass {

    static char ID; // Pass identification, replacement for typeid
    SimplifiedCFG() : ModulePass(ID) {}
    bool runOnModule(Module &M) override;
    void handleCall(CallSite);

  };
}


void SimplifiedCFG::handleCall(CallSite cs) {
    if(!cs.getInstruction())
        return;

    const DebugLoc &location = cs.getInstruction()->getDebugLoc();
    auto called = dyn_cast<Function>(cs.getCalledValue()->stripPointerCasts()); 

    if(!called)
        errs() << location.getLine() << ":" << location.getCol() << ":i\n";
    else if (!called->isIntrinsic()){
        if (called->getName() == "getWakeLock" || called->getName() == "freeWakeLock"){
            unsigned comp = dyn_cast<ConstantInt>(cs.getArgOperand(0))->getZExtValue();
            errs() << location.getLine() << ":" << location.getCol() << ":c:" << called->getName() << " " << comp << "\n";
        } else {
            errs() << location.getLine() << ":" << location.getCol() << ":c:" << called->getName() << "\n";
        }
    }
}

bool SimplifiedCFG::runOnModule(Module &M){
    
    // ---- N FUNC -----
    int numFunc = 0;
    for (auto& f : M){
        if (!f.isIntrinsic())
            ++numFunc;
    }
    errs() << numFunc <<  "\n";
    // ---------------

    for (auto& f : M){
        if (!f.isIntrinsic()){
            // ---- FUN INFO ----
            unsigned id = 0;
            unsigned numEdge = 0;
            for (BasicBlock& bb : f) {
                bb.setName(Twine(++id));
                for (succ_iterator SI = succ_begin(&bb), SE = succ_end(&bb); SI != SE; ++SI){
                    ++numEdge;
                }
            }
            errs() << f.getName() << " " << f.size() << " " << numEdge <<  "\n";
            for (BasicBlock& bb : f) {
                unsigned numCall = 0;
                // ---- BB INFO ----
                for (auto& i : bb) {
                    if(CallSite(&i).getInstruction()){
                        auto called = dyn_cast<Function>(CallSite(&i).getCalledValue()->stripPointerCasts()); 
                        if (!called || (called && !called->isIntrinsic())) // indirect or dircto not to dbg fun
                            ++numCall;
                    }
                }
                errs() << bb.getName() << " " << numCall <<  "\n";
                // ---- CALL INFO ----
                for (auto& i : bb) {
                    handleCall(CallSite(&i));
                }
                // ---------------
            }
            // ---- EDGE ----
            for (BasicBlock& bb : f) {
                for (succ_iterator SI = succ_begin(&bb), SE = succ_end(&bb); SI != SE; ++SI){
                    errs() << bb.getName() << " -> " << (*SI)->getName() <<  "\n";
                }
            }
            // ---------------
        }
    }

}

char SimplifiedCFG::ID = 0;
static RegisterPass<SimplifiedCFG> X("simplified-cfg", "build simplified cfg");

