No-Sleep Energy Bugs Detector
=============================

Building with CMake
==============================================
0. Clone the demo repository.

        git clone git@gitlab.com:rafaelmsoares/no-sleep-bug-detector.git

1. Change into the repository

        cd no-sleep-bug-detector

2. Create a new directory for building.

        mkdir build

3. Change into the new directory.

        cd build

4. Run CMake with the path to the project source.

        cmake ..

5. Run make inside the build directory:

        make

Running
==============================================

Suppose you have a program prog.c, Running the bug detector:

    scripts/bugDetector.sh prog.c

or by loading files from pass1 and pass2:

    scripts/bugDetector.sh -cfg prog.ll.cfgout prog.i

Running Pass1&2 only:

    scripts/makeCFG.sh prog.c

